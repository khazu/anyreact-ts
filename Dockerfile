# pull official base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install axios --save --silent
RUN npm install @syncfusion/ej2-react-schedule --save
RUN npm install react-router-dom history react-app-rewired --save



